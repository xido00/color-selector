import { default as reducer, initialState } from './index';
import * as actions from '../actions';

describe('reducer tests', () => {
  it('reducer handles BACKGROUND.SET action', () => {
    const background = 'f7f7f7';
    expect(
      reducer(initialState, actions.setBackground({ background }))
    ).toEqual({
      ...initialState,
      background
    });
  });

  it('reducer habdkes SUGGESTIONS.REQUEST action', () => {
    expect(
      reducer(initialState, actions.requestSuggestions())
    ).toEqual({
      ...initialState,
      isLoading: true
    })
  });

  it('reducer handles SUGGESTIONS.SUCCESS action', () => {
    const suggestions = [
      {
        name: 'aliceblue',
        hex: 'f0f8ff'
      },
      {
        name: 'antiquewhite',
        hex: 'faebd7'
      },
      {
        name: 'aqua',
        hex: '00ffff'
      }
    ];

    expect(
      reducer(initialState, actions.successSuggestions(suggestions))
    ).toEqual({
      ...initialState,
      isLoading: false,
      suggestions
    })
  });

  it('reducer handles SUGGESTIONS.FAILURE action', () => {
    const error = 'Error messsage';
    expect(
      reducer(initialState, actions.failureSuggestions(error))
    ).toEqual({
      ...initialState,
      isLoading: false,
      error
    })
  });
})
