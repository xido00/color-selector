import React, { Component } from 'react';
import Background from './components/background';
import Autosuggest from './components/autosuggest';

import {
  findColour,
  getDisplayValue,
  renderColourItem
} from './utils';

import './App.css';

class App extends Component {

  render() {
    const props = {
      filterSuggestions: findColour,
      getDisplayValue: getDisplayValue,
      renderItem: renderColourItem
    };

    return (
      <Background>
        <div className="wrapper">
          <h1>Colour Selector</h1>
          <p>Find your colour and change the background.</p>
          <p>You can search by colour name or hex code.</p>
          <Autosuggest {...props} />
        </div>
      </Background>
    );
  }
}

export default App;
