import { call, put, takeLatest } from 'redux-saga/effects';
import {
  types,
  successSuggestions,
  failureSuggestions
} from '../actions';

export const API_ADDRESS = 'http://www.mocky.io/v2/5a37a7403200000f10eb6a2d';

export function fetchSuggestionsSaga() {
  return fetch(API_ADDRESS).then(r => r.json());
}

export function* requestSuggestionsSaga() {
  try {
    const suggestions = yield call(fetchSuggestionsSaga);
    yield put(successSuggestions(suggestions));
  } catch (e) {
    yield put(failureSuggestions(e.message))
  }
}

export function* rootSaga() {
  yield takeLatest(types.SUGGESTIONS.REQUEST, requestSuggestionsSaga);
}

export default rootSaga;
