import * as utils from './index';

describe('Tests utils functions', () => {
  it('hexToRgb without arguments should return 0,0,0', () => {
    expect(utils.hexToRgb()).toEqual('0,0,0')
  });

  it('hexToRgb("#f7f7f7") should return 247,247,247', () => {
    expect(utils.hexToRgb('#f7f7f7')).toEqual('247,247,247')
  })

  it('hexToRgb("") should return 0,0,0', () => {
    expect(utils.hexToRgb('')).toEqual('0,0,0')
  })

  it('findColour should return true', () => {
    const colour = {
      hex: 'ffffff',
      name: 'white'
    };
    expect(utils.findColour('whi', colour)).toBeTruthy();
    expect(utils.findColour('fff', colour)).toBeTruthy();
  })
})
