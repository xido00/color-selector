import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { hexToRgb } from '../utils';

export class Background extends PureComponent {
  render() {
    const { colour, opacity } = this.props;
    const rgb = hexToRgb(colour);
    const props = {
      style: {
        background: `rgba(${rgb}, ${opacity})`
      }
    };

    return (
      <div className="background" { ...props }>
        { this.props.children }
      </div>
    );
  }

  static propTypes = {
    colour: PropTypes.string,
    opacity: PropTypes.number
  }

  static defaultProps = {
    opacity: 0.5
  }
}

const BackgroundLink = connect(state => ({
  colour: state.background
}), {})(Background);

export default BackgroundLink;
