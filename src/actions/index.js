export const types = {
  BACKGROUND: {
    SET: 'BACKGROUD.SET'
  },
  SUGGESTIONS: {
    REQUEST: 'SUGGESTIONS.REQUEST',
    SUCCESS: 'SUGGESTIONS.SUCCESS',
    FAILURE: 'SUGGESTIONS.FAILURE'
  }
};

export const setBackground = background => ({
  type: types.BACKGROUND.SET,
  ...background
});

export const requestSuggestions = () => ({
  type: types.SUGGESTIONS.REQUEST
})

export const successSuggestions = suggestions => ({
  type: types.SUGGESTIONS.SUCCESS,
  suggestions
})

export const failureSuggestions = error => ({
  type: types.SUGGESTIONS.FAILURE,
  error
})
