import { call, put, fork, takeLatest } from 'redux-saga/effects';
import {
  fetchSuggestionsSaga,
  requestSuggestionsSaga,
  rootSaga
} from './index';
import {
  types,
  requestSuggestions,
  successSuggestions,
  failureSuggestions
} from '../actions';

const message = 'error';
const suggestions = [
  {
    name: 'aliceblue',
    hex: 'f0f8ff'
  },
  {
    name: 'antiquewhite',
    hex: 'faebd7'
  },
  {
    name: 'aqua',
    hex: '00ffff'
  }
];

it('tests requestSuggestionsSaga generator', () => {
  const g = requestSuggestionsSaga();
  expect(g.next().value).toEqual(call(fetchSuggestionsSaga));
  expect(g.next().value).toEqual(put(successSuggestions()));
  expect(g.throw({ message }).value).toEqual(put(failureSuggestions(message)));
});

it('tests rootSaga', () => {
  const g = rootSaga();
  const expected = JSON.stringify(
    fork(takeLatest, types.SUGGESTIONS.REQUEST, requestSuggestionsSaga)
  );
  expect(JSON.stringify(g.next().value)).toEqual(expected);
})
