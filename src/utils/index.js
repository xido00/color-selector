import React from 'react';

export function hexToRgb(hex) {
  const rgb = [0, 0, 0]; // black is set as default

  if (typeof hex === 'string' && hex.length) {
    let bigint = parseInt(hex.replace('#', ''), 16);
    rgb[0] = (bigint >> 16) & 255;
    rgb[1] = (bigint >> 8) & 255;
    rgb[2] = bigint & 255;
  }

  return rgb.join();
}

export function findColour(query, colour) {
  const { name, hex } = colour;

  if (name.indexOf(query) >= 0) return true;

  if (hex.indexOf(query) >= 0) return true;

  return false;
}

export function debounce(callback, time) {
  let timeout;

  return (...args) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      timeout = null;
      callback(...args)
    }, time);
  }
}

export function getDisplayValue(item = {}) {
  return item.name || '';
}

export function renderColourItem(item, index, handleOnClick) {
  return (
    <li key={index} data-index={index} onClick={handleOnClick}>
      <span style={{backgroundColor: `#${item.hex}`}}></span>
      {item.name}
    </li>
  )
}
