import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { setBackground, requestSuggestions } from '../actions';
import { debounce } from '../utils';

export class Autosuggest extends Component {
  constructor(props) {
    super(props);

    this.state = {
      filtered: [],
      itemIsSelected: false,
      listIsCollapsed: true
    }

    this.buttonClickHandle = this.buttonClickHandle.bind(this);
    this.getSuggestions = this.getSuggestions.bind(this);
    this.selectSuggestion = this.selectSuggestion.bind(this);
  }

  buttonClickHandle(e) {
    const { itemIsSelected, filtered } = this.state;
    if (itemIsSelected === false ) return;
    this.props.onSelect(filtered[0]);
  }

  getSuggestions() {
    const { suggestions, isLoading, minQueryLength } = this.props;
    const value = this.input.value.trim().toLowerCase();

    if (value.length < minQueryLength) {
      return this.setState({
        listIsCollapsed: true,
        filtered: []
      });
    }

    // Fetchs suggestions if is empty
    if (!isLoading && suggestions.length === 0) {
      return this.props.fetchSuggestions();
    }

    const filtered = suggestions.filter(item =>
      this.props.filterSuggestions(value, item)
    );

    this.setState({
      listIsCollapsed: false,
      filtered
    });
  }

  componentDidUpdate(prevProps) {
    // Runs getSuggestions if received new suggestions
    const { suggestions, isLoading } = this.props;
    const oldSuggestions = prevProps.suggestions;
    if (!isLoading && oldSuggestions.length < suggestions.length) {
      this.getSuggestions();
    }
  }

  renderSuggestions() {
    const { filtered } = this.state;

    return filtered.map((v, i) =>
      this.props.renderItem(v, i, this.selectSuggestion
    ));
  }

  selectSuggestion(event) {
    const { target } = event;
    const { filtered } = this.state;
    const selectedItem = target.getAttribute('data-index');
    const item = filtered[selectedItem];
    const value = this.props.getDisplayValue(item);

    this.input.value = value;

    this.setState({
      itemIsSelected: true,
      listIsCollapsed: true,
      filtered: [item]
    });
  }

  render() {
    const { debounceTime } = this.props;
    const { listIsCollapsed } = this.state;
    return (
      <div className="autosuggest">
        <div className="autosuggest__input">
          <input
            type="text"
            name="autosuggest"
            placeholder="Select your favorite colour"
            onChange={ debounce(this.getSuggestions, debounceTime) }
            ref={ (input) => { this.input = input } }
          />
          {
            !listIsCollapsed &&
            <ul className="autosuggest__input__list" >
              { this.renderSuggestions() }
            </ul>
          }
        </div>
        <button type="button" onClick={this.buttonClickHandle}>
          Set background
        </button>
      </div>
    )
  }

  static propTypes = {
    debounceTime: PropTypes.number,
    fetchSuggestions: PropTypes.func,
    filterSuggestions: PropTypes.func.isRequired,
    getDisplayValue: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    minQueryLength: PropTypes.number,
    onSelect: PropTypes.func,
    renderItem: PropTypes.func.isRequired,
    suggestions: PropTypes.array.isRequired
  }

  static defaultProps = {
    debounceTime: 100,
    minQueryLength: 2,
    onSelect: () => {}
  }
}

const mapDispatchToProps = dispatch => ({
  onSelect: (colour) => {
    dispatch(setBackground({
      background: colour.hex
    }))
  },
  fetchSuggestions: () => {
    dispatch(requestSuggestions())
  }
});

const AutosuggestLink = connect(state => ({
  isLoading: state.isLoading,
  suggestions: state.suggestions
}), mapDispatchToProps)(Autosuggest);

export default AutosuggestLink;
