import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Autosuggest } from './autosuggest';

import { findColour, getDisplayValue, renderColourItem } from '../utils';

const suggestions = [
  {
    name: 'aliceblue',
    hex: 'f0f8ff'
  },
  {
    name: 'antiquewhite',
    hex: 'faebd7'
  },
  {
    name: 'aqua',
    hex: '00ffff'
  }
];

const setup = (_props = {}) => {
  const props = {
    suggestions,
    isLoading: false,
    fetchSuggestions: jest.fn(),
    filterSuggestions: findColour,
    getDisplayValue: getDisplayValue,
    renderItem: renderColourItem
  };

  Enzyme.configure({ adapter: new Adapter() });
  const wrapper = mount(<Autosuggest {...props} />)

  return {
    props,
    wrapper
  }
};

describe('<Autosuggest />', () => {
  it('should render self', () => {
    const { wrapper } = setup();
    expect(wrapper.find('.autosuggest').length).toBe(1);
  });

  it('should render list with filtered suggestions', () => {
    const { wrapper, props } = setup();
    wrapper.setState({
      filtered: props.suggestions,
      listIsCollapsed: false
    });
    expect(wrapper.find('.autosuggest__input__list').children().length)
      .toBe(props.suggestions.length);
  });

  it('should render input with name of selected item', () => {
    const { wrapper, props } = setup();
    const selected = props.suggestions[0];
    wrapper.setState({
      filtered: props.suggestions,
      listIsCollapsed: false
    });
    wrapper.find('.autosuggest__input__list li').at(0).simulate('click');
    const input = wrapper.find('.autosuggest__input input');
    expect(input.instance().value).toBe(selected.name);
  })
})
