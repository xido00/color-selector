import { types } from '../actions';

export const initialState = {
  background: 'ffffff',
  error: '',
  isLoading: false,
  suggestions: []
}

export function reducer(state = initialState, action) {
  switch(action.type) {
    case types.BACKGROUND.SET:
      return {
       ...state,
       background: action.background
      }

    case types.SUGGESTIONS.REQUEST:
      return {
        ...state,
        isLoading: true
      }

    case types.SUGGESTIONS.SUCCESS:
      return {
        ...state,
        isLoading: false,
        suggestions: action.suggestions
      }

    case types.SUGGESTIONS.FAILURE:
      return {
        ...state,
        isLoading: false,
        error: action.error,
      }

    default:
      return state
  }
}

export default reducer;
