import React from 'react';
import Enzyme, { mount } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import { Background } from './background';

import { hexToRgb } from '../utils';

const setup = (_props = {}) => {
  const props = {
    colour: 'f7f7f7',
    opacity: 0.5
  };

  Enzyme.configure({ adapter: new Adapter() });
  const wrapper = mount(<Background {...props} />)

  return {
    props,
    wrapper
  }
};

describe('<Background() />', () => {
  it('should render self', () => {
    const { wrapper } = setup();
    expect(wrapper.find('.background').length).toBe(1);
  });

  it('should render proper background color', () => {
    const { wrapper, props } = setup();
    const { colour, opacity } = props;
    const rgb = hexToRgb(colour);
    const styles = wrapper.find('.background').prop('style');
    expect(styles).toHaveProperty('background', `rgba(${rgb}, ${opacity})`);
  });
})
